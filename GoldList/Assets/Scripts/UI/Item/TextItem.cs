using TMPro;
using UnityEngine;

public class TextItem : MonoBehaviour
{
    private RectTransform _rect;
    private TextMeshProUGUI _text;

    public void Init(string text)
    {
        _rect = this.gameObject.transform.RectTransform();
        _text = this.gameObject.GetGoComponent<TextMeshProUGUI>("Text");
        _text.SetText(text);
        _rect.sizeDelta = new Vector2(_text.preferredWidth + 25, _text.preferredHeight + 5);
    }
}