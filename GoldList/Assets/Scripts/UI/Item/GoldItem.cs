using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GoldItem : MonoBehaviour
{
    private Transform _date;
    private Transform _gram;
    private Transform _price;

    private GoldData _goldData;
    private Action<GoldData> _action;

    private void Awake()
    {
        _date = gameObject.GetGOComponent("Date").transform;
        _gram = gameObject.GetGOComponent("Gram").transform;
        _price = gameObject.GetGOComponent("Price").transform;
    }

    public void Init(GoldData data, Action<GoldData> action)
    {
        _action = action;
        var go_0 = Instantiate(Resources.Load<GameObject>("Prefabs/TextItem"), _date);
        var go_1 = Instantiate(Resources.Load<GameObject>("Prefabs/TextItem"), _gram);
        var go_2 = Instantiate(Resources.Load<GameObject>("Prefabs/TextItem"), _price);
        go_0.GetComponent<TextItem>().Init($"{data.date}");
        go_1.GetComponent<TextItem>().Init($"{data.gram}");
        go_2.GetComponent<TextItem>().Init($"{data.price}");

        _goldData = data;

        transform.GetComponent<Button>().onClick.AddListener(OnGoldButtonClick);
    }

    void OnGoldButtonClick()
    {
        _action?.Invoke(_goldData);
    }
}