using System.Collections.Generic;
using AshFramework.Const;
using UnityEngine;
using UnityEngine.UI;

public class WeightListPanel : BasePanel
{
    private Transform _content;
    private Button _closeButton;
    private PoolManager<GoldItem> _goldItemPool;
    private List<GoldItem> _goldItems;

    void Awake()
    {
        _content = gameObject.GetGoComponent<Transform>("Content");
        _closeButton = gameObject.GetGoComponent<Button>("CloseButton");
        _closeButton.onClick.AddListener(OnCloseButtonClick);

        _goldItemPool = new PoolManager<GoldItem>(CreateGoldItem, item => item.gameObject.SetActive(false));
        _goldItems = new List<GoldItem>();
    }

    GoldItem CreateGoldItem()
    {
        var prefab = Resources.Load<GameObject>(Constants.Prefab + "GoldItem");
        var go = Instantiate(prefab, _content);
        var item = go.GetComponent<GoldItem>();
        return item;
    }

    public override void OnEnter(params object[] values)
    {
        gameObject.SetActive(true);

        _goldItemPool = new PoolManager<GoldItem>(CreateGoldItem, item => item.gameObject.SetActive(false));

        for (int i = 0; i < DataManager.Instance.GoldDatas.Count; i++)
        {
            var item = _goldItemPool.Get();
            // item.Init(DataManager.Instance.GoldDatas[i]);
            _goldItems.Add(item);
        }
    }

    public override void OnExit()
    {
        gameObject.SetActive(false);

        for (int i = 0; i < _goldItems.Count; i++)
        {
            _goldItemPool.Release(_goldItems[i]);
        }

        _goldItems.Clear();
    }

    public void OnCloseButtonClick()
    {
        // UIManager.Instance.PopPanel();
    }
}