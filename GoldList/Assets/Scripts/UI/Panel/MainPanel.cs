using System.Collections;
using System.Collections.Generic;
using AshFramework.Const;
using UnityEngine;
using UnityEngine.UI;

public class MainPanel : BasePanel
{
    private Button _bookButton;
    private Button _movieButton;
    private Button _goldButton;
    private Button _weightButton;

    void Start()
    {
        _bookButton = gameObject.GetGoComponent<Button>("BookButton");
        _movieButton = gameObject.GetGoComponent<Button>("MovieButton");
        _goldButton = gameObject.GetGoComponent<Button>("GoldButton");
        _weightButton = gameObject.GetGoComponent<Button>("WeightButton");

        _bookButton.onClick.AddListener(OnBookButtonClick);
        _movieButton.onClick.AddListener(OnMovieButtonClick);
        _goldButton.onClick.AddListener(OnGoldButtonClick);
        _weightButton.onClick.AddListener(OnWeightButtonClick);
    }

    private void OnBookButtonClick()
    {
    }

    private void OnMovieButtonClick()
    {
    }

    private void OnGoldButtonClick()
    {
        UIManager.Instance.ShowPanel(Constants.PathGoldEditPanel);
    }

    private void OnWeightButtonClick()
    {
        UIManager.Instance.ShowPanel(Constants.PathWeightListPanel);
    }
}