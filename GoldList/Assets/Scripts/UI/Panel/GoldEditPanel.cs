using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GoldEditPanel : BasePanel
{
    private TMP_InputField _dateInputField;
    private TMP_InputField _priceInputField;
    private TMP_InputField _gramInputField;
    private TextMeshProUGUI _title;

    private GoldData _goldData = null;

    void Awake()
    {
        _dateInputField = transform.FindChildByName("DateInputField").GetComponent<TMP_InputField>();
        _priceInputField = this.gameObject.GetGoComponent<TMP_InputField>("PriceInputField");
        _gramInputField = this.gameObject.GetGoComponent<TMP_InputField>("GramInputField");
        _title = this.gameObject.GetGoComponent<TextMeshProUGUI>("Title");

        this.gameObject.GetGoComponent<Button>("DateButton").onClick.AddListener(OnDateButtonClick);
        this.gameObject.GetGoComponent<Button>("SaveButton").onClick.AddListener(OnSaveButtonClick);
        this.gameObject.GetGoComponent<Button>("CloseButton").onClick.AddListener(OnCloseButtonClick);
    }

    public override void OnEnter(params object[] values)
    {
        _goldData = (GoldData)values[0];

        _dateInputField.text = _goldData.date;
        _priceInputField.text = $"{_goldData.price}";
        _gramInputField.text = $"{_goldData.gram}";
    }

    private void OnSaveButtonClick()
    {
        if (string.IsNullOrEmpty(_dateInputField.text))
        {
            Debug.LogError("日期为空。");
            return;
        }

        if (string.IsNullOrEmpty(_priceInputField.text))
        {
            Debug.LogError("价格为空。");
            return;
        }

        if (string.IsNullOrEmpty(_gramInputField.text))
        {
            Debug.LogError("克重为空。");
            return;
        }

        var data = new GoldData(_dateInputField.text, _priceInputField.text, _gramInputField.text);
        DataManager.Instance.GoldDatas.Add(data);
        DataManager.Instance.SaveGoldData();
    }

    private void OnDateButtonClick()
    {
        _dateInputField.text = DateTime.Now.ToShortDateString();
    }

    private void OnCloseButtonClick()
    {
        OnExit();
    }

    public override void OnExit()
    {
        this.gameObject.SetActive(false);
    }
}