using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolsManager : MonoBehaviour
{
    private static PoolsManager _instance;
    public static PoolsManager Instance => _instance ??= new PoolsManager();
}