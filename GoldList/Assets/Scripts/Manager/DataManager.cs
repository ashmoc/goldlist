using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class DataManager
{
    private static DataManager _instance;
    public static DataManager Instance => _instance ??= new DataManager();

    public static string TargetPath = $"{Application.streamingAssetsPath}/Datas";

    public List<GoldData> GoldDatas = new List<GoldData>();

    public DataManager()
    {
        GoldDatas.Clear();
        GoldDatas = SerializeList.ListFromJson<GoldData>(GetFile("GoldData.json"));
    }

    public void SaveData()
    {
        SaveGoldData();
    }

    public void SaveGoldData()
    {
        var jsonStr = SerializeList.ListToJson(GoldDatas, true);
        SaveFile("GoldData.json", jsonStr);
    }

    public static string GetFile(string fileName)
    {
        TargetPath.CheckAndCreateDirectory();
        string fullPath = System.IO.Path.Combine(TargetPath, fileName);
        if (!File.Exists(fullPath)) return string.Empty;
        var jsonStr = File.ReadAllText(fullPath);
        return jsonStr;
    }

    public static void SaveFile(string fileName, string jsonStr)
    {
        TargetPath.CheckAndCreateDirectory();
        string fullPath = System.IO.Path.Combine(TargetPath, fileName);

        if (!File.Exists(fullPath))
        {
            FileStream fs = new FileStream(fullPath, FileMode.Create, FileAccess.Write); //创建写入文件 
            StreamWriter sw = new StreamWriter(fs);
            sw.WriteLine(jsonStr);
            sw.Close();
            fs.Close();
        }
        else
        {
            File.WriteAllText(fullPath, jsonStr);
        }

#if UNITY_EDITOR
        UnityEditor.AssetDatabase.Refresh();
#endif
    }
}