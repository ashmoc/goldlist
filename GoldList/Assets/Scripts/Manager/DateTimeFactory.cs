using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using UnityEngine;

public static class DateTimeFactory
{
    //DateTime.Now.ToString()	    2016/5/9 13:09:55	短日期 长时间
    //DateTime.Now.ToString("d")    2016/5/9	短日期
    //DateTime.Now.ToString("D")	2016年5月9日	长日期
    //DateTime.Now.ToString("f")	2016年5月9日 13:09	长日期 短时间
    //DateTime.Now.ToString("F")	2016年5月9日 13:09:55	长日期 长时间
    //DateTime.Now.ToString("g")	2016/5/9 13:09	短日期 短时间
    //DateTime.Now.ToString("G") 	2016/5/9 13:09:55	短日期 长时间
    //DateTime.Now.ToString("t")	13:09	短时间
    //DateTime.Now.ToString("T")	13:09:55	长时间
    //DateTime.Now.ToString("u")	2016-05-09 13:09:55Z	 
    //DateTime.Now.ToString("U")	2016年5月9日 5:09:55	本初子午线的长日期和长时间                  
    //DateTime.Now.ToString("m")    5月9日	 
    //DateTime.Now.ToString("M")    5月9日	 
    //DateTime.Now.ToString("r")    Mon, 09 May 2016 13:09:55 GMT               	 
    //DateTime.Now.ToString("R")    Mon, 09 May 2016 13:09:55 GMT	 
    //DateTime.Now.ToString("y")	2016年5月	 
    //DateTime.Now.ToString("Y")	2016年5月	 
    //DateTime.Now.ToString("o")	2016-05-09T13:09:55.2350000	 
    //DateTime.Now.ToString("O")	2016-05-09T13:09:55.2350000	 
    //DateTime.Now.ToString("s")	2016-05-09T13:09:55	 

    public static DateTime GetDate(int year, int month, int day)
    {
        return new DateTime(year, month, day);
    }

    /// <summary>2016/5/9	短日期</summary>
    public static DateTime StrToDate(string dateStr)
    {
        string[] dateStrs = dateStr.Split('/');
        DateTime date = new DateTime(Convert.ToInt32(dateStrs[0]), Convert.ToInt32(dateStrs[1]), Convert.ToInt32(dateStrs[2]));
        return date;
    }

    /// <summary>2016/5/9	短日期</summary>
    public static string DateToShortString(this DateTime dateTime) => dateTime.ToString("d");
    
}