using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DataFactory
{
    public static string LoadData(string path)
    {
        TextAsset textAsset = Resources.Load($"{path}") as TextAsset;
        return textAsset.text;
    }

    public static void CreateTxt(string txtName, string txtContent)
    {
        //Z:/Projects/Unity/GoldList/goldlist/GoldList/Assets/av.txt
        string path = $"{Application.dataPath}/Resources/Data/{txtName}.txt";

        // //C:/Users/ashmoc/AppData/LocalLow/DefaultCompany/My project/av.txt
        // string path = $"{Application.persistentDataPath}/{txtName}.txt";

        // 文件流创建一个文本文件
        FileStream file = new FileStream(path, FileMode.Create);
        //得到字符串的UTF8 数据流
        byte[] bts = System.Text.Encoding.UTF8.GetBytes(txtContent);

        file.Write(bts, 0, bts.Length); // 文件写入数据流
        if (file != null)
        {
            file.Flush(); //清空缓存
            file.Close(); // 关闭流
            file.Dispose(); //销毁资源
        }
    }

    public static void WriteLineToTXT(string path, string content)
    {
        path = $"{Application.dataPath}/Resources/{path}.txt";

        StreamWriter streamWriter;
        FileInfo fileInfo = new FileInfo(path);

        streamWriter = File.Exists(path) ? fileInfo.AppendText() : fileInfo.CreateText();
        streamWriter.WriteLine(content);
        streamWriter.Close();
        streamWriter.Dispose();
    }

    public static void WriteContentToTXT(string path, string content)
    {
        path = $"{Application.dataPath}/Resources/{path}.txt";

        StreamWriter streamWriter;
        FileInfo fileInfo = new FileInfo(path);

        streamWriter = File.Exists(path) ? fileInfo.AppendText() : fileInfo.CreateText();
        streamWriter.Write(content);
        streamWriter.Close();
        streamWriter.Dispose();
    }

    public static void ReformatTXT(string path)
    {
        path = $"{Application.dataPath}/Resources/{path}.txt";

        // StreamWriter streamWriter;
        // FileInfo fileInfo = new FileInfo(path);
        //
        // streamWriter = File.Exists(path) ? fileInfo.AppendText() : fileInfo.CreateText();
        //
        // var list = content.Split("\n");
        // string newContent = string.Empty;
        // for (int i = 0; i < list.Length; i++)
        // {
        //     if (string.IsNullOrEmpty(list[i]) || string.IsNullOrWhiteSpace(list[i])) continue;
        //     newContent += $"{list[i]}\n";
        // }
        //
        // Debug.LogError(newContent);
        //
        // streamWriter.Write(content);
        //
        // streamWriter.Close();
        // streamWriter.Dispose();
    }


    /// <summary>
    /// 可以重新写入文件到一个已存在的txt文本中去
    /// </summary>
    static void ReWriteMyTxtByFileStreamTxt()
    {
        string path = Application.dataPath + "/Json/MyTxtByFileStream.txt";

        string[] strs = { "123", "321", "234" };

        File.WriteAllLines(path, strs);
    }

    /// <summary>
    /// 第三种读取方法
    /// 需引用  using System.Text;
    /// </summary>
    static void ReadTxtThird()
    {
        string path = Application.dataPath + "/Json/MyTxtByFileInfo.txt";

        string str = File.ReadAllText(path, Encoding.UTF8);

        //第二种方法添加txt文本
    }

    /// <summary>
    /// 第四种读取方法
    /// </summary>
    static void ReadTxtForth()
    {
        string path = Application.dataPath + "/Json/MyTxtByFileInfo.txt";
        //FileStream fsSource = new FileStream(path, FileMode.Open, FileAccess.Read);

        //文件读写流
        StreamReader strr = new StreamReader(path);
        //读取内容
        string str = strr.ReadToEnd();

        //第二种方法添加txt文本
    }


    /// <summary>
    /// 第五种读取方法
    /// 文件流方式
    /// </summary>
    static void ReadTxtFifth()
    {
        string path = Application.dataPath + "/Json/MyTxtByFileInfo.txt";
        FileStream files = new FileStream(path, FileMode.Open, FileAccess.Read);
        byte[] bytes = new byte[files.Length];
        files.Read(bytes, 0, bytes.Length);
        files.Close();
        string str = UTF8Encoding.UTF8.GetString(bytes);
        //第二种方法添加txt文本
    }
}