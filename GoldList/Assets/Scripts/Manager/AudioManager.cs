using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    private static AudioManager _instance;

    public static AudioManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new AudioManager();
            }

            return _instance;
        }
    }

    private Dictionary<string, AudioClip> _adDic = new Dictionary<string, AudioClip>();

    public AudioClip LoadAudio(string path, bool cache = false)
    {
        AudioClip au = null;
        if (!_adDic.TryGetValue(path, out au))
        {
            au = Resources.Load<AudioClip>(path);
            if (cache)
            {
                _adDic.Add(path, au);
            }
        }

        return au;
    }
}