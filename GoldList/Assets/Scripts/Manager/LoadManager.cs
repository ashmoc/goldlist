using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadManager : MonoBehaviour
{
    private static LoadManager _instance;

    public static LoadManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new LoadManager();
            }

            return _instance;
        }
    }

    private Dictionary<string, GameObject> _goDic = new Dictionary<string, GameObject>();

    public GameObject LoadPrefab(string path, bool cache = false)
    {
        GameObject prefab = null;
        if (!_goDic.TryGetValue(path, out prefab))
        {
            prefab = Resources.Load<GameObject>(path);
            if (cache)
            {
                _goDic.Add(path, prefab);
            }
        }

        GameObject go = null;
        if (prefab != null)
        {
            go = Instantiate(prefab);
        }

        return go;
    }

    private Dictionary<string, Sprite> spDic = new Dictionary<string, Sprite>();

    public Sprite LoadSprite(string path, bool cache = false)
    {
        Sprite sp = null;
        if (!spDic.TryGetValue(path, out sp))
        {
            sp = Resources.Load<Sprite>(path);
            if (cache)
            {
                spDic.Add(path, sp);
            }
        }

        return sp;
    }
}