using System;
using System.Collections.Generic;
using UnityEngine;

public class SerializeList
{
    public static string ListToJson<T>(List<T> list, bool prettyPrint = false) =>
        JsonUtility.ToJson(new SerializationList<T>(list), prettyPrint);

    public static List<T> ListFromJson<T>(string str) => JsonUtility.FromJson<SerializationList<T>>(str).ToList();
}

[Serializable]
public class SerializationList<T>
{
    [SerializeField] private List<T> list;
    public SerializationList(List<T> list) => this.list = list;
    public List<T> ToList() => list;
}