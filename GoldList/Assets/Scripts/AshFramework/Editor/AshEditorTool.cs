﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace AshFramework.Editor
{
    public class AshEditorTool : MonoBehaviour
    {
        [MenuItem("AshTools/清除本地缓存", false, 99)]
        private static void ClearPlayerPrefs()
        {
            PlayerPrefs.DeleteAll();
            PlayerPrefs.Save();
            Debug.Log("所有本地缓存已清除");
        }

        [MenuItem("AshTools/启用Start场景为默认启动场景", priority = -999)]
        public static void ToggleAutoStartScene()
        {
            AutoStartScene.IsEnabled = !AutoStartScene.IsEnabled;
        }

        [MenuItem("AshTools/启用Start场景为默认启动场景", true)]
        public static bool ToggleAutoStartSceneValidate()
        {
            Menu.SetChecked("AshTools/启用Start场景为默认启动场景", AutoStartScene.IsEnabled);
            return true;
        }
    }

    [InitializeOnLoad]
    public static class AutoStartScene
    {
        private const string EnableKey = "AutoStartSceneEnabled";
        private static string _previousScenePath;

        // 属性：是否启用自动加载场景
        public static bool IsEnabled
        {
            get => EditorPrefs.GetBool(EnableKey, true);
            set => EditorPrefs.SetBool(EnableKey, value);
        }

        // 静态构造函数，Unity 启动时注册回调
        static AutoStartScene()
        {
            EditorApplication.playModeStateChanged += OnPlayModeStateChanged;
        }

        private static void OnPlayModeStateChanged(PlayModeStateChange state)
        {
            if (!IsEnabled) return;

            if (state == PlayModeStateChange.ExitingEditMode)
            {
                // 保存当前活动场景路径
                _previousScenePath = EditorSceneManager.GetActiveScene().path;

                // 指定你希望运行的场景名称
                string startSceneName = "Start";

                // 获取场景的完整路径
                string[] guids = AssetDatabase.FindAssets($"t:Scene {startSceneName}");
                if (guids.Length > 0)
                {
                    string scenePath = AssetDatabase.GUIDToAssetPath(guids[0]);

                    // 加载指定的场景
                    if (!string.IsNullOrEmpty(scenePath))
                    {
                        Debug.Log($"Auto-loading Start scene: {scenePath}");
                        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
                        EditorSceneManager.OpenScene(scenePath);
                    }
                    else
                    {
                        Debug.LogError($"Start scene '{startSceneName}' not found!");
                    }
                }
                else
                {
                    Debug.LogError($"Start scene '{startSceneName}' not found in the project!");
                }
            }
            else if (state == PlayModeStateChange.EnteredEditMode)
            {
                // 回到之前的场景
                if (!string.IsNullOrEmpty(_previousScenePath))
                {
                    Debug.Log($"Returning to previous scene: {_previousScenePath}");
                    EditorSceneManager.OpenScene(_previousScenePath);
                    _previousScenePath = null;
                }
            }
        }


        [SettingsProvider]
        public static SettingsProvider CreateSettingsProvider()
        {
            return new SettingsProvider("Project/AutoStartScene", SettingsScope.Project)
            {
                label = "Auto Start Scene",
                guiHandler = (searchContext) =>
                {
                    IsEnabled = EditorGUILayout.Toggle("Enable Auto Start Scene", IsEnabled);
                }
            };
        }
    }
}