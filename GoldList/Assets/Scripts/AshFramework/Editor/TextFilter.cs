﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

/// <summary>去除重复的文字，用于减小 TextMesh Pro 生成文件的大小。</summary>
public class TextFilter
{
    static string inputFileName = "TextMesh Pro/Fonts/Text.txt";

    [MenuItem("AshTools/获取所有脚本文本", false, 0)]
    static void GetProjectAllScriptText()
    {
        // 读取整个文件内容
        string fileContent = File.ReadAllText(GetTextPath(), System.Text.Encoding.UTF8);
        
        string[] guids = AssetDatabase.FindAssets("t:Script");
        foreach (string guid in guids)
        {
            string path = AssetDatabase.GUIDToAssetPath(guid);
            fileContent += GetScriptText(path);
        }

        ProcessText(fileContent);
    }

    private static string GetTextPath()
    {
        // 获取输入文件的完整路径
        string filePath = Path.Combine(Application.dataPath, inputFileName);

        if (!File.Exists(filePath))
        {
            Debug.LogError($"文件不存在：{filePath}");
        }

        return filePath;
    }

    private static void ProcessText(string content)
    {
        // 逐字检查并除去重复字符串
        string processedContent = RemoveDuplicates(content);

        // 将处理后的内容保存到输出文件中
        File.WriteAllText(GetTextPath(), processedContent);

        Debug.Log($"成功：[{GetTextPath()}]");
    }

    private static string RemoveDuplicates(string inputText)
    {
        if (string.IsNullOrEmpty(inputText))
            return string.Empty;

        StringBuilder result = new StringBuilder();
        HashSet<char> seenCharacters = new HashSet<char>();
        inputText += GetCommonCharacter();

        foreach (char currentChar in inputText)
        {
            if (!seenCharacters.Contains(currentChar))
            {
                result.Append(currentChar);
                seenCharacters.Add(currentChar);
            }
        }

        return result.ToString();
    }

    private static string GetCommonCharacter()
    {
        StringBuilder result = new StringBuilder();

        // 获取基本符号
        for (int code = 33; code <= 126; code++)
        {
            char character = (char)code;
            result.Append(character);
        }

        return result.ToString();
    }

    #region 读取某个脚本中的所有文字

    public static string GetScriptText(string scriptPath)
    {
        MonoScript script = AssetDatabase.LoadAssetAtPath<MonoScript>(scriptPath);

        if (script != null)
        {
            return script.text;
        }
        else
        {
            Debug.LogError("没有找到此文件： " + scriptPath);
            return string.Empty;
        }
    }

    #endregion

    [MenuItem("AshTools/获取场景组件文本", false, 1)]
    public static void GetSceneComponentText()
    {
        string content = "";

        content += File.ReadAllText(GetTextPath(), System.Text.Encoding.UTF8);

        Text[] text = GameObject.FindObjectsOfType<Text>();
        foreach (Text t in text)
        {
            content += t.text;
        }

        TextMeshProUGUI[] tmp = GameObject.FindObjectsOfType<TextMeshProUGUI>();
        foreach (TextMeshProUGUI t in tmp)
        {
            content += t.text;
        }

        ProcessText(content);
    }
}