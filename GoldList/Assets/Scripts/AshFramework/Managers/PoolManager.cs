using System;
using System.Collections.Generic;
using UnityEngine;

namespace AshFramework
{
    public interface IPool<T>
    {
        T Allocate();
        bool Recycle(T go);
    }

    public abstract class Pool<T> : IPool<T>
    {
        protected Stack<T> CacheStack = new Stack<T>();

        protected IGOFactory<T> Factory;

        public int Count => CacheStack.Count;

        public T Allocate()
        {
            return CacheStack.Count > 0 ? CacheStack.Pop() : Factory.Create();
        }

        public abstract bool Recycle(T go);
    }

    public class PoolManager<T> : Pool<T>
    {
        private Action<T> _resetAction;

        public PoolManager(Func<T> factoryFunc, Action<T> resetAction = null, int initCount = 0)
        {
            _resetAction = resetAction;
            Factory = new GOFactory<T>(factoryFunc);

            for (int i = 0; i < initCount; ++i)
            {
                CacheStack.Push(Factory.Create());
            }
        }

        public override bool Recycle(T go)
        {
            _resetAction?.Invoke(go);
            CacheStack.Push(go);
            return true;
        }
    }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  

    public interface IGOFactory<T>
    {
        T Create();
    }

    public class GOFactory<T> : IGOFactory<T>
    {
        private Func<T> _func;

        public GOFactory(Func<T> func)
        {
            _func = func;
        }

        public T Create()
        {
            return _func();
        }
    }
}