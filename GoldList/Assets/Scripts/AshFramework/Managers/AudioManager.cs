using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AshFramework
{
    public class AudioManager : MonoSingleton<AudioManager>
    {
        private AudioListener _audioListener;

        public void PlaySound(string name)
        {
            CheckAudioListener();

            var clip = Resources.Load<AudioClip>(name);
            var audioSource = gameObject.AddComponent<AudioSource>();

            audioSource.clip = clip;
            audioSource.Play();
        }

        private AudioSource _musicSource;

        public void PlayMusic(string name, bool loop)
        {
            CheckAudioListener();

            if (!_musicSource)
            {
                _musicSource = gameObject.AddComponent<AudioSource>();
            }

            var clip = Resources.Load<AudioClip>(name);
            _musicSource.clip = clip;
            _musicSource.loop = loop;
            _musicSource.Play();
        }

        public void StopMusic()
        {
            _musicSource.Stop();
        }

        public void PauseMusic()
        {
            _musicSource.Pause();
        }

        public void ResumeMusic()
        {
            _musicSource.UnPause();
        }

        public void MusicOn()
        {
            _musicSource.UnPause();
            _musicSource.mute = false;
        }

        public void MusicOff()
        {
            _musicSource.Pause();
            _musicSource.mute = true;
        }

        public void SoundOn()
        {
            var audioSources = GetComponents<AudioSource>();

            foreach (var audioSource in audioSources)
            {
                if (audioSource != _musicSource)
                {
                    audioSource.UnPause();
                    audioSource.mute = false;
                }
            }
        }

        public void SoundOff()
        {
            var audioSources = GetComponents<AudioSource>();

            foreach (var audioSource in audioSources)
            {
                if (audioSource != _musicSource)
                {
                    audioSource.Pause();
                    audioSource.mute = true;
                }
            }
        }

        private void CheckAudioListener()
        {
            if (!_audioListener)
            {
                _audioListener = FindObjectOfType<AudioListener>();
            }

            if (!_audioListener)
            {
                _audioListener = gameObject.AddComponent<AudioListener>();
            }
        }
    }
}