﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace AshFramework.Managers
{
    public class ObjectPoolManager : MonoBehaviour
    {
        // 单例模式
        private static ObjectPoolManager _instance;

        public static ObjectPoolManager Instance
        {
            get
            {
                // 如果没有实例，创建一个并设置为常驻节点
                if (_instance == null)
                {
                    GameObject persistentObject = new GameObject("ObjectPoolManager");
                    _instance = persistentObject.AddComponent<ObjectPoolManager>();
                    DontDestroyOnLoad(persistentObject); // 设置为常驻节点
                }

                return _instance;
            }
            set => _instance = value;
        }

        private Dictionary<string, Queue<GameObject>> _pool;

        private void Awake()
        {
            // 只需要在这里初始化对象池字典
            if (_instance == null)
            {
                _instance = this;
                _pool = new Dictionary<string, Queue<GameObject>>();
            }
            else
            {
                Destroy(gameObject); // 如果已有实例，销毁当前物体
            }
        }

        public void CreatePool(string path, int count)
        {
            if (!_pool.ContainsKey(path)) _pool.Add(path, new Queue<GameObject>());

            var prefab = Resources.Load<GameObject>(path);
            if (prefab == null)
            {
                Debug.LogError($"找不到该物体：{path}");
                return;
            }

            if (_pool[path].Count < count)
            {
                for (int i = 0; i < count - _pool[path].Count; i++)
                {
                    var go = Instantiate(prefab);
                    go.name = path.Split('/').Last();

                    _pool[path].Enqueue(go);
                    go.SetActive(false);
                    go.transform.SetParent(transform);
                }
            }
        }

        public GameObject GetGo(string path, Transform parent)
        {
            if (!_pool.ContainsKey(path)) _pool.Add(path, new Queue<GameObject>());

            GameObject go;

            if (_pool[path].Count > 0)
            {
                go = _pool[path].Dequeue();
                go.SetActive(true);
                go.transform.SetParent(parent);
            }
            else
            {
                var prefab = Resources.Load<GameObject>(path);
                if (prefab == null)
                {
                    Debug.LogError($"找不到该物体：{path}");
                    return null;
                }

                go = Instantiate(prefab);
                go.name = path.Split('/').Last();
                go.transform.SetParent(parent);
            }

            return go;
        }

        public void ReturnGo(GameObject go)
        {
            var poolKey = "";
            foreach (string key in _pool.Keys)
            {
                var name = key.Split('/').Last();
                if (name == go.name) poolKey = key;
            }

            if (!_pool.ContainsKey(poolKey))
            {
                Debug.LogError($"对象池中没有：{go.name}");
                Destroy(go);
                return;
            }

            go.SetActive(false);
            go.transform.SetParent(transform);
            _pool[poolKey].Enqueue(go);
        }
    }
}