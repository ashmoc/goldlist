using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefsManager
{
    public static void SetString(string key, string value)
    {
        PlayerPrefs.SetString(key, value);
        PlayerPrefs.Save();
    }

    public static string GetString(string key)
    {
        return PlayerPrefs.GetString(key);
    }

    public static void SetBool(string key, bool value)
    {
        PlayerPrefs.SetInt(key, value ? 1 : 0);
        PlayerPrefs.Save();

// #if UNITY_EDITOR
//         PlayerPrefs.SetInt(key, value ? 1 : 0);
//         PlayerPrefs.Save();
// #else
//         PrefsManager.SetInt(key, value ? 1 : 0);
// #endif
    }

    public static bool GetBool(string key)
    {
        return PlayerPrefs.GetInt(key, 0) == 1;
// #if UNITY_EDITOR
//         return PlayerPrefs.GetInt(key, 0) == 1;
// #else
//         // return PrefsManager.GetInt(key) == 1;
// #endif
    }

    public static void SetInt(string key, int value)
    {
        PlayerPrefs.SetInt(key, value);
        PlayerPrefs.Save();
// #if UNITY_EDITOR
//         PlayerPrefs.SetInt(key, value);
//         PlayerPrefs.Save();
// #else
//          // QG.StorageSetIntSync(key, value);
// #endif
    }

    public static int GetInt(string key, int defaultValue = 0)
    {
        return PlayerPrefs.GetInt(key, defaultValue);
// #if UNITY_EDITOR
//         return PlayerPrefs.GetInt(key);
// #else
//         // return QG.StorageGetIntSync(key, 0);
// #endif
    }

    public static void SetFloat(string key, float value)
    {
        PlayerPrefs.SetFloat(key, value);
        PlayerPrefs.Save();
    }

    public static float GetFloat(string key)
    {
        return PlayerPrefs.GetFloat(key);
    }
}