﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class UIManager : MonoBehaviour
{
    private static UIManager _instance;

    public static UIManager Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject persistentObject = new GameObject("UIManager");
                _instance = persistentObject.AddComponent<UIManager>();
            }

            return _instance;
        }
        set => _instance = value;
    }

    private Dictionary<string, GameObject> _uiDic = new Dictionary<string, GameObject>();

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            _uiDic = new Dictionary<string, GameObject>();
        }
        else Destroy(gameObject);
    }

    public GameObject ShowPanel(string path)
    {
        if (!_uiDic.ContainsKey(path))
        {
            var prefab = Resources.Load<GameObject>(path);
            GameObject go = Instantiate(prefab, FindObjectOfType<Canvas>().transform);
            go.name = path.Split('/').Last();
            _uiDic.Add(path, go);
        }

        _uiDic[path].SetActive(true);
        return _uiDic[path];
    }

    public void HidePanel(string uiName)
    {
        if (_uiDic.TryGetValue(uiName, out GameObject uiInstance))
        {
            uiInstance.SetActive(false);
        }
        else
        {
            Debug.LogWarning($"UI {uiName} is not active!");
        }
    }
}