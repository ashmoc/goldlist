using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AshFramework
{
    public abstract class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
    {
        protected static T instance = null;

        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<T>();
                    if (FindObjectsOfType<T>().Length > 1)
                    {
                        Debug.LogWarning($"{typeof(T).Name} more than 1.");
                        return instance;
                    }

                    if (instance == null)
                    {
                        var instanceName = typeof(T).Name;
                        Debug.Log($"Instance name {instanceName}");

                        var instanceGO = GameObject.Find(instanceName) == null
                            ? new GameObject(instanceName)
                            : GameObject.Find(instanceName);

                        instance = instanceGO.AddComponent<T>();
                        DontDestroyOnLoad(instanceGO);
                        Debug.Log($"Add new Singleton {instanceName} in game.");
                    }
                    else
                    {
                        Debug.Log($"Already exist: {instance.name}");
                    }
                }

                return instance;
            }
        }

        protected virtual void OnDestroy() => instance = null;
    }
}