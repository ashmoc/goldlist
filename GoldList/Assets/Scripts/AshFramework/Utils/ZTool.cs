using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;
using Random = UnityEngine.Random;

public static class ZTool
{
    public static GameObject GetGOComponent(this GameObject gameObject, string name)
    {
        try
        {
            return FindChildRecursively(gameObject.transform, name);
        }
        catch (Exception e)
        {
            Debug.LogError($"未找到物体 \n[Name] {name}\n{e}");
            return null;
        }
    }

    public static RectTransform RectTransform(this Transform transform) => transform.GetComponent<RectTransform>();

    public static T GetGoComponent<T>(this GameObject gameObject, string name) where T : UnityEngine.Component
    {
        try
        {
            T go = FindChildRecursively<T>(gameObject.transform, name);
            if (go != null)
            {
                return go;
            }
            else
            {
                Debug.LogError($"未找到物体: {name} Type: {typeof(T).Name}");
                return null;
            }
        }
        catch (Exception e)
        {
            Debug.LogError($"未找到物体 \n[Name] {name} Type: {typeof(T).Name}\n{e}");
            return null;
        }
    }

    private static T FindChildRecursively<T>(Transform root, string name) where T : UnityEngine.Component
    {
        if (root.name.Equals(name))
        {
            T t = root.GetComponent<T>();
            if (t != null)
            {
                return t;
            }
        }

        for (int i = 0; i < root.childCount; i++)
        {
            T t = FindChildRecursively<T>(root.GetChild(i), name);
            if (t != null)
            {
                return t;
            }
        }

        return null;
    }

    private static GameObject FindChildRecursively(Transform root, string name)
    {
        if (root.name.Equals(name))
        {
            return root.gameObject;
        }

        for (int i = 0; i < root.childCount; i++)
        {
            GameObject gameObject = FindChildRecursively(root.GetChild(i), name);
            if (!object.ReferenceEquals(gameObject, null))
            {
                return gameObject;
            }
        }

        return null;
    }

    public static TValue TryGet<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key)
    {
        TValue value;
        dictionary.TryGetValue(key, out value);
        return value;
    }

    /// <summary>
    /// 查找子物体，按名称查找
    /// </summary>
    /// <param name="parent">父物体的Transform</param>
    /// <param name="name">子物体的名称</param>
    /// <returns>返回找到的子物体的Transform，如果没有找到则返回null</returns>
    public static Transform FindChildByName(this Transform parent, string name)
    {
        // 遍历所有子物体
        foreach (Transform child in parent)
        {
            if (child.name == name)
            {
                return child;
            }

            // 如果子物体有子物体，递归查找
            Transform found = FindChildByName(child, name);
            if (found != null)
            {
                return found;
            }
        }

        // 没有找到指定名称的子物体
        return null;
    }

    public static T DeepCopy<T>(T obj)
    {
        string json = JsonUtility.ToJson(obj);
        return JsonUtility.FromJson<T>(json);
    }

    #region Common

    /// <summary>复制文本到剪切板</summary>
    public static void CopyText(string text) => GUIUtility.systemCopyBuffer = $"{text}";

    public static Color GetColorFormHex(string hexColor)
    {
        Color color;
        if (ColorUtility.TryParseHtmlString(hexColor, out color))
        {
            return color;
        }

        Debug.LogError("无效的 16 进制颜色代码：" + hexColor);
        return Color.white;
    }

    #endregion

    #region IO

    public static string CheckAndCreateDirectory(this string path)
    {
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }

        return path;
    }

    #endregion

    #region Transform

    /// <summary>把世界坐标转成屏幕坐标</summary>
    public static Vector2 WorldToScreenPoint(Canvas canvas, Vector3 world)
    {
        Vector2 screenPoint = RectTransformUtility.WorldToScreenPoint(canvas.worldCamera, world);
        Vector2 localPoint;
        //把屏幕坐标转成局部坐标
        RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.GetComponent<RectTransform>(), screenPoint,
            canvas.worldCamera,
            out localPoint);

        return localPoint;
    }

    public static void Identity(this Transform transform)
    {
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        transform.localScale = Vector3.one;
    }

    public static void SetLocalPosX(this Transform transform, float x) =>
        transform.localPosition = new Vector3(x, transform.localPosition.y, transform.localPosition.z);

    public static void SetLocalPosY(this Transform transform, float y) =>
        transform.localPosition = new Vector3(transform.localPosition.x, y, transform.localPosition.z);

    public static void SetLocalPosZ(this Transform transform, float z) =>
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, z);

    public static void SetLocalPosXY(this Transform transform, float x, float y) =>
        transform.localPosition = new Vector3(x, y, transform.localPosition.z);

    public static void SetLocalPosXZ(this Transform transform, float x, float z) =>
        transform.localPosition = new Vector3(x, transform.localPosition.y, z);

    public static void SetLocalPosYZ(this Transform transform, float y, float z) =>
        transform.localPosition = new Vector3(transform.localPosition.x, y, z);

    public static void AddChild(this Transform transform, Transform child) => child.SetParent(transform);

    public static bool IsPointInRect(Transform trans, Vector2 point)
    {
        return RectTransformUtility.RectangleContainsScreenPoint(trans.GetComponent<RectTransform>(), point);
    }

    #endregion

    #region Math

    public static bool IsInRange(int range, int minInclusive = 0, int maxExclusive = 100) =>
        Random.Range(minInclusive, maxExclusive) < range;

    public static T GetRandomFrom<T>(params T[] values) => values[Random.Range(0, values.Length)];

    /// <summary>
    /// 根据概率返回 true 或 false
    /// </summary>
    /// <param name="probability">概率值，范围为 0 到 1</param>
    /// <returns>如果小于等于概率返回 true，否则返回 false</returns>
    public static bool CheckProbability(float probability)
    {
        // 确保概率值在 0 到 1 的范围内
        probability = Mathf.Clamp01(probability);

        // 生成一个随机数并判断
        return Random.value <= probability;
    }

    #endregion

    #region Others

    public static bool InShieldTime()
    {
        if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday || DateTime.Now.DayOfWeek == DayOfWeek.Sunday) return false;
        if (DateTime.Now.Hour < 19 && DateTime.Now.Hour >= 9) return true;
        return false;
    }

    public static bool InTimeRange(int year, int month, int day, int hour, int minute = 0, int second = 0) =>
        DateTime.Now < new DateTime(year, month, day, hour, minute, second);

    public static string ToFormattedString(this float number)
    {
        if (number >= 1000000)
        {
            return (number / 1000000f).ToString("0.#") + "M"; //ToString("0.#")：格式化字符串，保留一位小数，但省略多余的零。
        }
        else if (number >= 1000)
        {
            return (number / 1000f).ToString("0.#") + "K";
        }
        else
        {
            return number.ToString("0");
        }
    }

    public static string ToFormattedString(this int number)
    {
        return ((float)number).ToFormattedString();
    }

    #endregion
}