﻿using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace AshFramework.UI
{
    public class Tip : MonoBehaviour
    {
        private RectTransform _background;
        private TextMeshProUGUI _text;

        private void Awake()
        {
            _background = transform.FindChildByName("Background").GetComponent<RectTransform>();
            _text = transform.FindChildByName("Text").GetComponent<TextMeshProUGUI>();
        }

        private void Show(string text)
        {
            _text.text = $"{text}";
            _background.sizeDelta = new Vector2(_text.preferredWidth + 100, _background.rect.height);
            transform.localScale = Vector3.zero;
            transform.DOScale(1, 0.3f).SetEase(Ease.OutBounce).OnComplete(() =>
            {
                var y = transform.position.y + 200;
                _text.DOFade(0, 0.5f).SetEase(Ease.InExpo).SetDelay(0.75f);
                _background.GetComponent<Image>().DOFade(0, 0.5f).SetEase(Ease.InExpo).SetDelay(0.75f);
                transform.DOMoveY(y, 0.5f).SetEase(Ease.OutQuad).SetDelay(0.75f)
                    .OnComplete(() => { Destroy(gameObject); });
            });
        }

        public static void ShowTip(string text)
        {
            var canvas = FindObjectOfType<Canvas>();

            if (canvas == null)
            {
                Debug.LogError("场景中没有找到 Canvas！");
                return;
            }

            GameObject prefab = Resources.Load<GameObject>("UI/Tip");
            if (prefab != null)
            {
                GameObject tip = Instantiate(prefab, canvas.transform);
                tip.GetComponent<Tip>().Show(text);
            }
            else
            {
                Debug.LogError("无法加载Tip预制体：UI/Tip");
            }
        }
    }
}