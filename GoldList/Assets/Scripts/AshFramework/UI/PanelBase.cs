using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PanelBase : MonoBehaviour
{
    // 显示面板的方法
    public void ShowPanel()
    {
        gameObject.SetActive(true);
        OnPanelShown();
    }

    // 隐藏面板的方法
    public void HidePanel()
    {
        gameObject.SetActive(false);
        OnPanelHidden();
    }

    protected virtual void OnPanelShown()
    {
        // 在面板展示时执行的操作
    }

    protected virtual void OnPanelHidden()
    {
        // 在面板隐藏时执行的操作
    }
}