﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public enum UICommonTweenType
{
    Rotate,
    Shake,
}

public class UICommonTweenAnimation : MonoBehaviour
{
    public UICommonTweenType type = UICommonTweenType.Rotate;

    public bool playOnLoad = true;

    public float speed = 1.5f;
    public float delay = 1.5f;

    private RectTransform _rect;

    private void Awake()
    {
        _rect = transform.GetComponent<RectTransform>();
    }

    private void Start()
    {
        if (playOnLoad) Play();
    }

    public void Play()
    {
        switch (type)
        {
            case UICommonTweenType.Rotate:
                _rect.DORotate(new Vector3(0, 0, -360), speed, RotateMode.FastBeyond360).SetEase(Ease.Linear)
                    .SetLoops(-1, LoopType.Restart);
                break;
            case UICommonTweenType.Shake:
                transform.DOKill();
                transform.DOShakePosition(speed, new Vector3(30, 0, 0), 10, 90, false, true)
                    .SetDelay(delay)
                    .SetLoops(-1, LoopType.Restart);
                break;
        }
    }
}