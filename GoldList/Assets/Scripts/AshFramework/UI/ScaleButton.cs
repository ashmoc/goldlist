﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;

public class ScaleButton : Button
{
    [SerializeField] private Vector3 pressScale = new Vector3(0.9f, 0.9f, 0.9f); // 按下时的缩放
    [SerializeField] private Vector3 releaseScale = Vector3.one; // 抬起时的缩放
    [SerializeField] private float pressDuration = 0.1f; // 按下时动画持续时间
    [SerializeField] private float releaseDuration = 0.2f; // 抬起时动画持续时间
    [SerializeField] private Ease pressEase = Ease.OutQuad; // 按下动画的缓动
    [SerializeField] private Ease releaseEase = Ease.OutFlash; // 抬起动画的缓动

    protected override void Awake()
    {
        base.Awake();
        pressScale = transform.localScale * 0.9f;
        releaseScale = transform.localScale;
    }

    // 按下时触发的动画
    public override void OnPointerDown(PointerEventData eventData)
    {
        base.OnPointerDown(eventData); // 保留基础按钮行为

        // 按下缩放动画
        transform.DOScale(pressScale, pressDuration).SetEase(pressEase);
    }

    // 抬起时触发的动画
    public override void OnPointerUp(PointerEventData eventData)
    {
        base.OnPointerUp(eventData); // 保留基础按钮行为

        // 抬起缩放动画
        transform.DOScale(releaseScale, releaseDuration).SetEase(releaseEase);
    }
}