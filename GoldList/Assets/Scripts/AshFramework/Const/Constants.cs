using System.Collections.Generic;

namespace AshFramework.Const
{
    public static class Constants
    {
        public const string Name_GoldList = "GoldList";

        public const string COLOR_GOLD = "#d8ab4c";
        public const string COLOR_PURPLE = "#3c1e4b";
        public const string COLOR_PURPLE_LIGHT = "#634b6f";
        public const string COLOR_BULE_DARK = "#001e46";

        public const string Prefab = "Prefabs/";
        public const string GoldData = "Data/GoldList";

        public const string PathGoldEditPanel = "Prefabs/Panel/GoldListPanel";
        public const string PathWeightListPanel = "Prefabs/Panel/WeightListPanel";
    }
}