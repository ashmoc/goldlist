using System;

[Serializable]
public class GoldData
{
    public string date;
    public float price;
    public float gram;

    public GoldData(string date, string price, string gram)
    {
        this.date = date;
        this.price = float.Parse(price);
        this.gram = float.Parse(gram);
    }

    public GoldData(DateTime date, float price, float gram)
    {
        this.price = price;
        this.gram = gram;
    }

    public string ToString()
    {
        return $"{date}_{price}_{gram}";
    }
}